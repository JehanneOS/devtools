#!/bin/bash

# This file is part of Jehanne.
#
# Copyright (C) 2016-2017 Giacomo Tesio <giacomo@tesio.it>

BUILD_TRAMPOLINE=true
BUILD_DRAWTERM=true

while test $# -gt 0
do
    case "$1" in
        --help) echo "$0 [ --no-trampoline | --no-drawterm | --help ]"
		exit 0
            ;;
        --no-tools) BUILD_TRAMPOLINE=false
            ;;
        --no-drawterm) BUILD_DRAWTERM=false
            ;;
        --*) echo "bad option $1" && exit 1
            ;;
        *) echo "unexpected argument $1" && exit 1
            ;;
    esac
    shift
done

cd `dirname $0`
if [ -z "$UTILITIES" ]; then
	UTILITIES=`pwd`
fi
if [ "$BUILD_TRAMPOLINE$BUILD_DRAWTERM" = "truetrue" ]; then
	git clean -x -d -f $UTILITIES/bin
fi
if [ "$BUILD_TRAMPOLINE" = "true" ]; then
	echo -n Building development tools.
	(
		# Inside parentheses, and therefore a subshell . . .
		while [ 1 ]   # Endless loop.
		do
		  echo -n "."
		  sleep 3
		done
	) &
	dotter=$!
	(
		(cd $UTILITIES/src/trampoline/ && ./build.sh)
		(cd $UTILITIES/src/netcat && XLIB=-lresolv DFLAGS=-DGAPING_SECURITY_HOLE make linux && mv nc $UTILITIES/bin && git clean -xdf .)
		(cd $UTILITIES/src/u9fs && make && mv u9fs $UTILITIES/bin && git clean -xdf .)
	) > $UTILITIES/src/utils.build.log 2>&1
	STATUS="$?"
	kill $dotter
	wait $dotter 2>/dev/null
	if [ ! $STATUS -eq "0" ]
	then
		echo "FAIL"
		cat utils.build.log
		exit $STATUS
	else
		rm $UTILITIES/src/utils.build.log
		echo "done."
	fi
fi

if [ "$BUILD_DRAWTERM" = "true" ]; then
	echo -n Building drawterm.
	(
		# Inside parentheses, and therefore a subshell . . .
		while [ 1 ]   # Endless loop.
		do
		  echo -n "."
		  sleep 3
		done
	) &
	dotter=$!
	(
		cd $UTILITIES/src/drawterm/ && 
		git clean -xdf > ../drawterm.build.log 2>&1 &&
		CONF=unix make >> ../drawterm.build.log 2>&1 &&
		mv drawterm $UTILITIES/bin
	)
	STATUS="$?"
	kill $dotter
	wait $dotter 2>/dev/null
	if [ $STATUS -eq "0" ]
	then
		rm $UTILITIES/src/drawterm.build.log
		echo "done."
	else
		echo "FAIL"
		cat $UTILITIES/src/drawterm.build.log
		exit $STATUS
	fi
fi
exit $STATUS
