#!/bin/sh
if [ "$U9FS_LOGGING" = "enable" ]; then
	LOGFILE=$JEHANNE/hacking/lib/u9fs.log
	date > $LOGFILE
	echo Starting U9FS >> $LOGFILE
	U9FS_LOG="-l $LOGFILE -z -D"
fi
$JEHANNE/hacking/bin/u9fs $U9FS_LOG -a none -u $USER $JEHANNE
