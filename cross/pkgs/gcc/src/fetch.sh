#!/bin/sh -e

download () {
	TARGET=$(basename $1)
	if [ -f $TARGET ]; then
		echo "fetch: skip $TARGET (already present)"
	elif command -v curl >/dev/null 2>&1; then
		curl -s --fail "$1" > $TARGET
	elif command -v wget >/dev/null 2>&1; then
		wget -O- "$1" > $TARGET
	else
		echo "ERROR: Unable to find either curl or wget" >&2
		exit 1
	fi
}

extract () {
	TARGET=$1
	ARCHIVE=$2
	if [ -d $TARGET ]; then
		echo "fetch: skip $TARGET (already present)"
	else
		mkdir -p $TARGET
		(cd $TARGET && tar xf ../$ARCHIVE --strip-components=1)
	fi
}

echo "fetch: downloading..."
download 'https://gmplib.org/download/gmp/gmp-6.1.2.tar.bz2'
download 'https://www.mpfr.org/mpfr-4.0.1/mpfr-4.0.1.tar.bz2'
download 'https://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz'
download 'http://ftp.gnu.org/gnu/binutils/binutils-2.33.1.tar.bz2'
download 'https://ftp.gnu.org/gnu/gcc/gcc-9.2.0/gcc-9.2.0.tar.gz'

echo "fetch: check sum..."
sha256sum -c sha256sum.txt

echo "fetch: extract archive..."
extract libgmp gmp-6.1.2.tar.bz2
extract libmpfr mpfr-4.0.1.tar.bz2
extract libmpc mpc-1.1.0.tar.gz
extract binutils binutils-2.33.1.tar.bz2
extract gcc gcc-9.2.0.tar.gz

echo "fetch: done."
